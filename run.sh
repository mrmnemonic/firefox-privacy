#!/bin/bash

if [ -z "${FFXPROFILE}" ]; then
FFXPROFILE=ffxuser
fi

PWD=$(pwd)
echo "Working from $PWD"
echo "Using profile: ${FFXPROFILE}"

# Is there already a profile?
if [ ! -d "${PWD}/${FFXPROFILE}" ]; then
echo "Creating new Firefox profile"
mkdir ${FFXPROFILE}/
firefox/firefox -CreateProfile "${FFXPROFILE} ${PWD}/${FFXPROFILE}"
cp user.js ${PWD}/${FFXPROFILE}/
fi

if [ "${PRIVATE}" == 'y' ]; then
echo "Launching Private Firefox"
firefox/firefox -private-window -profile "${PWD}/${FFXPROFILE}"
else
echo "Launching Firefox"
firefox/firefox -profile "${PWD}/${FFXPROFILE}"
fi

exit
