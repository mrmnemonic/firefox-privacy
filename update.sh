#!/bin/bash

FFLANG=en-GB
FFARCH=64
FFCHANNEL=latest-ssl

VERSION=${VERSION:-$(wget --spider -S --max-redirect 0 "https://download.mozilla.org/?product=firefox-${FFCHANNEL}&os=linux${FFARCH}&lang=${FFLANG}" 2>&1 | sed -n '/Location: /{s|.*/firefox-\(.*\)\.tar.*|\1|p;q;}')}

if [ -f "firefox-${VERSION}.tar.bz2" ]; then
	echo "Already have ${VERSION}"
	exit
fi

echo "Fetching Firefox ${VERSION}"

# Fetch latest firefox
echo -n "Fetching Firefox..."
wget -nv --no-clobber --continue --content-disposition 'https://download.mozilla.org/?product=firefox-latest-ssl&os=linux64&lang=en-GB'
echo "Done"

if [ -d ./firefox/ ]; then
echo "Previous Firefox detected, deleting..."
rm -Rf firefox/
fi

echo -n "Extracting Firefox..."
tar jxf firefox-${VERSION}.tar.bz2
echo "Done"

exit
